---
layout: en/post
title: New Masster and  Bachelor students join OCMP group.
date: 2018.04.01.10:00:00
categories: news
---

We welcome new master student Mr. Atsushi Haizuka from Fukui University. We also welcome new Bachelor students Mr. Akira Kawano and Mr. Shunsuke Taniwaki to our group.
