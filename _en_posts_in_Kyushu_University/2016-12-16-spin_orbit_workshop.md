---
layout: en/post
title: Prof. Satoh presented an Invited talk at Korea-Japan Spin-Orbit Workshop.
date: 2016.12.16.10:00:00
categories: news
---

The title of Prof. Satoh’s talk is “Optical excitation of coupled spin-orbit dynamics in antiferromagnetic CoO”.
