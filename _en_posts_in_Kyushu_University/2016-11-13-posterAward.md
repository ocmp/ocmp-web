---
layout: en/post
title: Masters Student Mr. Kosei Himeno received the Best Poster Award.
date: 2016.11.13.10:00:00
categories: award
---

Mr. Kosei Himeno received the best Poster Presentation Award in International workshop on novel photo induced phenomena and applications. The title of the poster is “Spin wave propagation in rare-earth iron garnet”.
