---
layout: en/post
title: New Research and Bachelor students join OCMP group.
date: 2017.04.01.10:00:00
categories: news
---

We welcome International Research Rtudent Mr. Wei-Hunghsu from Taiwan in OCMP group. We also welcome new Bachelor students Mr. Katsuhiro Konno and Ms. Minori Kichise to our group.
