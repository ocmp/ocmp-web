---
layout: ja/default
title: Opto-magnonics&#x3a; light pulses manipulating spin waves
authors: T. Satoh
magazine:
  title: SPIE Newsroom (2013).
  url: http://www.spie.org/newsroom/technical-articles/4631-opto-magnonics-light-pulses-manipulating-spin-waves
pdf:
  title: PDF
  url: /assets/pdf/SPIE004631_10.pdf
date: 2013.01.21.10:00
categories: other_publications
---
