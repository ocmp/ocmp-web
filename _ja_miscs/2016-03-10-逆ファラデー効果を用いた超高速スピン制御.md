---
layout: ja/default
title: 逆ファラデー効果を用いた超高速スピン制御
authors: 佐藤琢哉
magazine:
  title: 応用物理 <b>85</b>, 9-14 (2016).
  url: https://doi.org/10.11470/oubutsu.85.1_9
date: 2016.03.10.10:00
categories: misc
---
