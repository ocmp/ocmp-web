---
layout: ja/default
title: Chiral phonons<b>:</b> circularly polarized Raman spectroscopy and ab initio calculations in a chiral crystal tellurium
authors: K. Ishito, H. Mao, K. Kobayashi, Y. Kousaka, Y. Togawa, H. Kusunose, J. Kishine, and T. Satoh
site_url: http://doi.org/10.1002/chir.23544
journal: Chirality
volume: 35
page: 338-345
year: 2023
pdf:
  title: [PDF]
  url: https://onlinelibrary.wiley.com/doi/epdf/10.1002/chir.23544
date: 2022.12.06.10:00
categories: publication
---

