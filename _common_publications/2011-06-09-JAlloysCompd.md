---
layout: ja/default
title: Crystallization and magnetic behavior of nanosized nickel ferrite prepared by citrate precursor method
authors: D. T. T. Nguyet, N. P. Duong, Le T. Hung, T. D. Hien, and T. Satoh
site_url: https://doi.org/10.1016/j.jallcom.2011.03.112
journal: J. Alloys Compd.
volume: 509
page: 6621-6625
year: 2011
date: 2011.06.09.10:00
categories: publication
---
