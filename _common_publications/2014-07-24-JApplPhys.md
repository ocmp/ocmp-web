---
layout: ja/default
title: Phase-controllable spin wave generation in iron garnet by linearly polarized light pulses
authors: I. Yoshimine, T. Satoh, R. Iida, A. Stupakiewicz, A. Maziewski, and T. Shimura
site_url: https://doi.org/10.1063/1.4891107
pdf:
  title: [PDF]
  url: https://catalog.lib.kyushu-u.ac.jp/opac_download_md/1808891/609083_0_merged_1401157869.pdf
journal: J. Appl. Phys.
volume: 116
page: 043907-1-8
year: 2014
date: 2014.07.24.10:00:00
categories: publication
---
