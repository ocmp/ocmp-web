---
layout: ja/default
title: Resonance-enhanced two-photon sum-frequency generation in NiO and KNiF<sub>3</sub>
authors: T. Satoh, Th. Lottermoser, and M. Fiebig
site_url: https://doi.org/10.1007/s00340-004-1615-2
journal: Appl. Phys. B
volume: 79
page: 701-706
year: 2004
date: 2004.07.15.10:00
categories: publication
---

Online Publishedに合わせました。

> First online: 15 September 2004