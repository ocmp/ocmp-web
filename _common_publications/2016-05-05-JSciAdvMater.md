---
layout: ja/default
title: Crystallization and magnetic characterizations of DyIG and HoIG nanopowders fabricated using citrate sol-gel
authors: D. T. T. Nguyet, N. P. Duong, T. Satoh, L. N. Anh, T. T. Loan, and T. D. Hien
site_url: https://doi.org/10.1016/j.jsamd.2016.05.005
journal: J. Sci. Adv. Mater. Dev.
volume: 1
page: 193-199
year: 2016
date: 2016.05.05.10:00
categories: publication
---
