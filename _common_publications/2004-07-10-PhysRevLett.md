---
layout: ja/default
title: Ultrafast manipulation of antiferromagnetism in NiO
authors: N. P. Duong, T. Satoh, and M. Fiebig
site_url: https://doi.org/10.1103/PhysRevLett.93.117402
journal: Phys. Rev. Lett.
volume: 93
page: 117402-1-4
year: 2004
pdf:
  title: [PDF]
  url: /assets/pdf/PRL93_117402_2004.pdf
date: 2004.07.10.10:00
categories: publication
---
