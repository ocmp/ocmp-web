---
layout: ja/default
title: Coherent control of antiferromagnetism in NiO
authors: T. Satoh, N. P. Duong, and M. Fiebig
site_url: https://doi.org/10.1103/PhysRevB.74.012404
journal: Phys. Rev. B
volume: 74
page: 012404-1-4
year: 2006
pdf:
  title: [PDF]
  url: /assets/pdf/PRB74_012404_2006.pdf
date: 2006.07.17.10:00
categories: publication
---
