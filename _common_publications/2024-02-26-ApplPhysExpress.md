---
layout: ja/default
title: Tesla-class single-cycle terahertz magnetic field pulses generated with a spiral-shaped metal microstructure
authors: K. Maruyama, Z. Zhang, M. Takumi, T. Satoh, M. Nakajima, Y. Kanemitsu, and  H. Hirori
site_url: https://iopscience.iop.org/article/10.35848/1882-0786/ad2909
journal: Appl. Phys. Express
volume: 17   
page: 022004-1-6
year: 2024
pdf:
  title: [PDF]
  url: https://iopscience.iop.org/article/10.35848/1882-0786/ad2909/pdf
date: 2024.02.26.10:00
categories: publication
---
