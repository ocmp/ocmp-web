---
layout: ja/default
title: Non-thermal optical excitation of terahertz-spin precession in a magneto-optical insulator
authors: S. Parchenko, T. Satoh, I. Yoshimine, F. Stobiecki, A. Maziewski, and A. Stupakiewicz
site_url: https://doi.org/10.1063/1.4940241
journal: Appl. Phys. Lett.
volume: 108
page: 032404-1-5
year: 2016
date: 2016.01.20.10:00
categories: publication
---

> Received 21 10月 2015 Accepted 08 1月 2016 Published online 20 1月 2016