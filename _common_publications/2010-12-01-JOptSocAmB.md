---
layout: ja/default
title: Generation of mid- to far-infrared ultrashort pulses in 4-dimethylamino-N-methyl-4-stilbazolium tosylate crystal
authors: T. Satoh, Y. Toya, S. Yamamoto, T. Shimura, K. Kuroda, Y. Takahashi, M. Yoshimura, Y. Mori, T. Sasaki, and S. Ashihara
journal: J. Opt. Soc. Am. B
volume: 27
page: 2507-2511
year: 2010
site_url: https://doi.org/10.1364/JOSAB.27.002507
date: 2010.12.01.10:00
categories: publication
---

下記のリンクから出版日を調べました。
issue 12は「12 December」に出版されている模様。
https://www.osapublishing.org/josab/issue.cfm?volume=27&issue=12