---
layout: ja/default
title: Photoinduced transient Faraday rotation in NiO
authors: T. Satoh, S.-J. Cho, T. Shimura, K. Kuroda, H. Ueda, Y. Ueda, and M. Fiebig
journal: J. Opt. Soc. Am. B
volume: 27
page: 1421-1424
year: 2010
site_url: https://doi.org/10.1364/JOSAB.27.001421
date: 2010.07.01.10:00
categories: publication
---

https://www.osapublishing.org/josab/issue.cfm?volume=27&issue=7