---
layout: ja/default
title: Sub-millimeter propagation of antiferromagnetic magnons via magnon-photon coupling
authors: R. Kainuma, K. Matsumoto, T. Ito, and T. Satoh
site_url: https://www.nature.com/articles/s44306-024-00034-3
journal: npj Spintronics
volume: 2
page: 31
year: 2024
pdf:
  title: [PDF]
  url: https://www.nature.com/articles/s44306-024-00034-3.pdf
date: 2024.07.05.10:00
categories: publication
---

