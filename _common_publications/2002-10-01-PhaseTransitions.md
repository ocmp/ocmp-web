---
layout: ja/default
title: Irreversible photoinduced insulator-metal transition in charge-ordered Pr<sub>0.75</sub>Na<sub>0.25</sub>MnO<sub>3</sub>
authors: T. Satoh, Y. Kikuchi, K. Miyano, E. Pollert, J. Hejtm&aacute;nek, and Z. Jir&aacute;k
site_url: https://doi.org/10.1080/01411590290034083
journal: Phase Transitions
volume: 75
page: 935-940
year: 2002
complement: Proceedings of International Conference on "Photoinduced Phase Transitions, their Dynamics and Precursor Phenomena" (Nov. 2001, Tsukuba, Japan).
date: 2002.10.01.10:00
categories: publication
---


Web of Science調べ

http://apps.webofknowledge.com/InboundService.do?UT=WOS%3A000180231000035&IsProductCode=Yes&mode=FullRecord&SID=S1jw3CwMGDYVp69F3cM&product=WOS&smartRedirect=yes&SrcApp=literatum&DestFail=http%3A%2F%2Fwww.webofknowledge.com%3FDestApp%3DCEL%26DestParams%3D%253Faction%253Dretrieve%2526mode%253DFullRecord%2526product%253DCEL%2526UT%253DWOS%253A000180231000035%2526customersID%253Datyponcel%26e%3Dvs69W9WSvUTIDLHn2hGsUniwKaaNxVuhArDebR0opxwgCr1NENqbQF7bLo9KyUBy%26SrcApp%3Dliteratum%26SrcAuth%3Datyponcel&Init=Yes&action=retrieve&SrcAuth=atyponcel&customersID=atyponcel&Func=Frame
