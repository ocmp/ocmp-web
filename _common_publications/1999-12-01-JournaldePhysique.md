---
layout: ja/default
title: "Photoinduced and current-driven insulator/metal transition in manganites: A fluctuating route"
authors: K. Miyano, T. Tonogai, T. Satoh, H. Oshima, and Y. Tokura
site_url: https://doi.org/10.1051/jp4:19991081
journal: Journal de Physique IV
volume: 9
page: 311-314
year: 1999
complement: Proceedings of International Workshop on Electronic Crystals ECRYS-99 (May 1999, Colle-sur-Loup, France).
date: 1999.12.01.10:00
categories: publication
---
