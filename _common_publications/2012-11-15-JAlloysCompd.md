---
layout: ja/default
title: Temperature-dependent magnetic properties of YIG nanoparticles prepared by citrate sol-gel
authors: D. T. T. Nguyet, N. P. Duong, T. Satoh, L. N. Anh, and T. D. Hien
site_url: https://doi.org/10.1016/j.jallcom.2012.06.122
journal: J. Alloys Compd.
volume: 541
page: 18-22
year: 2012
date: 2012.11.15.10:00
categories: publication
---
