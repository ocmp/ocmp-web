---
layout: ja/default
title: Detection of spin and charge states in centrosymmetric materials by nonlinear optics
authors: T. Satoh, Th. Lottermoser, M. Fiebig, Y. Ogimoto, H. Tamaru, M. Izumi, and K. Miyano
site_url: https://doi.org/10.1063/1.1851676
journal: J. Appl. Phys.
volume: 97
page: 10A914-1-3
year: 2005
date: 2005.05.02.10:00
proceeding: Proceedings of the 49th Annual Conference on Magnetism and Magnetic Materials (Nov. 2004, Jacksonville, USA).
categories: publication
---


Online Publishedに合わせました。

> http://scitation.aip.org/content/aip/journal/jap/97/10/10.1063/1.1851676