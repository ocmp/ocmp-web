---
layout: ja/default
title: Ultrafast optical excitation of coherent magnons in antiferromagnetic NiO
authors: C. Tzschaschel, K. Otani, R. Iida, T. Shimura, H. Ueda, S. G&uuml;nther, M. Fiebig, and T. Satoh
site_url: https://doi.org/10.1103/PhysRevB.95.174407
pdf:
  title: [PDF]
  url: /assets/pdf/PRB95_174407_2017.pdf
journal: Phys. Rev. B
volume: 95
page: 174407-1-11
year: 2017

date: 2017.01.20.10:00
categories: publication
---

> Received 21 10月 2015 Accepted 08 1月 2016 Published online 20 1月 2016
>pdf:
>  title: [PDF]
>  url: /assets/pdf/PRB95_174407.pdf
