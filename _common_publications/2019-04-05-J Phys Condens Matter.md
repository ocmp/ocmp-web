---
layout: ja/default
title: Excitation of coherent optical phonons in iron garnet by femtosecond laser pulses
authors: P. Khan, M. Kanamaru, W. H. Hsu, M. Kichise, Y. Fujii, A. Koreeda, and T. Satoh
site_url: https://doi.org/10.1088/1361-648X/ab1665
journal: J. Phys.&#58; Condens. Matter
volume: 31
page: 275402-1-8
year: 2019
pdf:
  title: [PDF]
  url: https://catalog.lib.kyushu-u.ac.jp/opac_download_md/2800487/2800487.pdf
date: 2019.04.05.10:00
categories: publication
---

> Received 21 10月 2015 Accepted 08 1月 2016 Published online 20 1月 2016