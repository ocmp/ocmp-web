---
layout: ja/default
title: Spin oscillations in antiferromagnetic NiO triggered by circularly polarized light
authors: T. Satoh, S.-J. Cho, R. Iida, T. Shimura, K. Kuroda, H. Ueda, Y. Ueda, B. A. Ivanov, F. Nori, and M. Fiebig
journal: Phys. Rev. Lett.
volume: 105
page: 077402-1-4
year: 2010
site_url: https://doi.org/10.1103/PhysRevLett.105.077402
date: 2010.08.11.10:00
pdf:
  title: [PDF]
  url: /assets/pdf/PRL105_077402_2010.pdf
categories: publication
---
