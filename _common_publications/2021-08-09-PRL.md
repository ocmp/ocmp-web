---
layout: ja/default
title: Ultrafast amplification and nonlinear magnetoelastic coupling of coherent magnon modes in an antiferromagnet
authors: D. Bossini, M. Pancaldi, L. Soumah, M. Basini, F. Mertens, M. Cinchetti, T. Satoh, O. Gomonay, and S. Bonetti
site_url: https://doi.org/10.1103/PhysRevLett.127.077202
journal: Phys. Rev. Lett.
volume: 127
page: 077202-1-6
year: 2021
pdf:
  title: [PDF]
  url: /assets/pdf/PRL127_077202_2021.pdf
date: 2021.08.09.10:00
categories: publication
---

Received 5 November 2020; revised 22 May 2021; accepted 24 June 2021; published 9 August 2021