---
layout: ja/default
title: Atomic order, magnetic and transport phenomena in half-Heusler CoMnSb<sub>0.9</sub>Z<sub>0.1</sub> alloys (Z = Si, Al, Sn, and Bi)
authors: N. P. Duong, D. T. T. Nguyet, L. N. Anh, T. T. Loan, and T. Satoh
site_url: https://doi.org/10.1016/j.ceramint.2023.09.356
journal: Ceram. Int. 
volume: 49
page: 40211-40220 
year: 2023
date: 2023.11.12.10:00
categories: publication
---
