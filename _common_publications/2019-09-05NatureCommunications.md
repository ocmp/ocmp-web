---
layout: ja/default
title: Tracking the ultrafast motion of an antiferromagnetic order parameter
authors: C. Tzschaschel, T. Satoh, and M. Fiebig 
site_url: https://doi.org/10.1038/s41467-019-11961-9
journal: Nature Commun.
volume: 10
page: 3995-1-6
year: 2019
pdf:
  title: [PDF]
  url: https://www.nature.com/articles/s41467-019-11961-9.pdf
date: 2019.09.05.10:00
categories: publication
---

> Received 21 10月 2015 Accepted 08 1月 2016 Published online 20 1月 2016