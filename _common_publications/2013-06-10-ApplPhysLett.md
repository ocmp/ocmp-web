---
layout: ja/default
title: Wide frequencies range of spin excitations in a rare-earth Bi-doped iron garnet with a giant Faraday rotation
authors: S. Parchenko, A. Stupakiewicz, I. Yoshimine, T. Satoh, and A. Maziewski
site_url: https://doi.org/10.1063/1.4826248
journal: Appl. Phys. Lett.
volume: 103
page: 172402-1-4
year: 2013
date: 2013.06.10.10:00
categories: publication
---
