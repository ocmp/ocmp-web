---
layout: ja/page
title: お問い合わせ
global_navi_ja: true
global_navi_en: false
permalink: /ja/contact/
---

佐藤研究室では、新しい研究分野を切り開く意志をもった卒研生、大学院生を募集しています。
理学院物理学系の大学院入学試験の案内は[こちら](https://www.titech.ac.jp/graduate_school/admissions/guide.html)です。
博士研究員を希望の方は、ご相談ください。
研究室見学を随時、歓迎しております。

{:refdef .contact-table}
|**連絡先**  |佐藤 琢哉（教授）|
|**所属**    |東京科学大学 理学院 物理学系 佐藤研究室|
|**住所**    |〒152-8551 東京都目黒区大岡山2-12-1　　|
||          南5号館5階　502D (教授室)　507CD (居室)|
|**電話番号(教授室)**|03-5734-2716|
|**電話番号(居室)**|03-5734-2454|
|**メール**  |![satoh](/assets/images/email/satoh-mail.png){: .email-image}|
{: refdef}

## アクセス

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d681.9677541882263!2d139.6839152787946!3d35.60297723491039!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f5312c64657f%3A0xc5d613e39931acf1!2z44CSMTQ1LTAwNjEg5p2x5Lqs6YO95aSn55Sw5Yy655-z5bed55S677yR5LiB55uu77yT77yRIOWkp-WyoeWxseWNlzXlj7fppKg!5e0!3m2!1sja!2sjp!4v1554266674683!5m2!1sja!2sjp" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
