---
layout: ja/post
title: 博士課程1年の貝沼凌さんが2022年度物質・情報卓越教育院国際フォーラム Good Presentation Award を受賞しました！
date: 2022.12.07.10:00:00
categories: award
---

## 参考リンク

1. [2022年度物質・情報卓越教育院国際フォーラム](https://www.tac-mi.titech.ac.jp/activity/2022-international-forum/)