---
layout: ja/post
title: 研究成果がプレスリリースされました。
date: 2022.11.10.10:00:00
categories: news
---

らせん結晶内で回転する原子の運動モードを観測。
詳細は[東工大Webページ](https://www.titech.ac.jp/news/2022/065276)をご覧ください。

