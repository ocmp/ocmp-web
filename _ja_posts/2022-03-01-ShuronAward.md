---
layout: ja/post
title: 修士課程2年の貝沼凌さんが令和3年度物理学コース優秀修士論文賞を受賞しました！
date: 2022.03.01.10:00:00
categories: award
---

## 参考リンク

1. [令和３年度物理学コース優秀修士論文賞](http://info.phys.sci.titech.ac.jp/news_detail.html?C1021/D4183)