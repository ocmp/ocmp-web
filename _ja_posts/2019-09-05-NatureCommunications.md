---
layout: ja/post
title: 新しい論文が Nature Communications に掲載されました。
date: 2019.09.05.10:00:00
categories: paper
---

## 参考リンク

1. [Nature Communications 10, 3995 (2019)](https://www.nature.com/articles/s41467-019-11961-9)
