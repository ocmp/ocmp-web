---
layout: ja/post
title: 新しい論文が Applied Physics Express に掲載されました。
date: 2024.02.26.10:00:00
categories: paper
---

## 参考リンク

1. [Applied Physics Express (2024)]( https://iopscience.iop.org/article/10.35848/1882-0786/ad2909)