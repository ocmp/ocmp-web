---
layout: ja/post
title: 修士課程2年の望月颯一郎さんが第85回応用物理学会秋季学術講演会の英語講演奨励賞を受賞しました！
date: 2024.12.12.10:00:00
categories: award
---

## 参考リンク

1. [第85回応用物理学会秋季学術講演会 第22回英語講演奨励賞](https://annex.jsap.or.jp/spintro/)
