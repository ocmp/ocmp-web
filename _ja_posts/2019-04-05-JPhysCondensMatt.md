---
layout: ja/post
title: 新しい論文が Journal of Physics&#58; Condensed Matter に掲載されました。
date: 2019.04.05.10:00:00
categories: paper
---

## 参考リンク

1. [J. Phys.&#58; Condens. Matter 31, 275402 (2019)](https://doi.org/10.1088/1361-648X/ab1665)
