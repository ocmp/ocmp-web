---
layout: ja/post
title: 修士課程2年の楠野楽到さんが令和6年度物理学コース優秀修士論文賞を受賞しました！
date: 2025.02.27.10:00:00
categories: award
---

## 参考リンク

1. [令和6年度物理学コース優秀修士論文賞](https://www.phys.sci.isct.ac.jp/wp/wp-content/uploads/2025/03/070305-%E4%BB%A4%E5%92%8C%EF%BC%96%E5%B9%B4%E5%BA%A6%E7%89%A9%E7%90%86%E5%AD%A6%E3%82%B3%E3%83%BC%E3%82%B9%E5%84%AA%E7%A7%80%E4%BF%AE%E2%BC%A0%E8%AB%96%E2%BD%82%E8%B3%9E.pdf)