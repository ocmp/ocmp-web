---
layout: ja/post
title: 新しい論文が Physical Review B に掲載されました。
date: 2020.05.06.10:00:00
categories: paper
---

## 参考リンク

1. [Physical Review B 101, 184407-1-6 (2020)](https://doi.org/10.1103/PhysRevB.101.184407)
