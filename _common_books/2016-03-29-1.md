---
layout: ja/default
title: Ultrafast Magnetism I
book_url: https://doi.org/10.1007/978-3-319-07743-7
authors: T. Satoh, Y. Terui, R. Moriya, B. A. Ivanov, K. Ando, E. Saitoh, T. Shimura, and K. Kuroda
body: Proceedings of the International Conference UMC 2013 Strasbourg, France, October 28th - November 1st, 2013 <br /> Series&#x3a; Springer Proceedings in Physics, Vol. 159 (2014) <br /> J.-Y. Bigot, W. Hübner, T. Rasing, R. Chantrell (Eds.) <br /> Excitation and control of spin wave by light pulses
categories: book
---
