---
layout: ja/default
meeting:
  title: Magnonics 2017
  place: Oxford, UK
  period: 7-10
first_presentation:
  title: Time-resolved imaging of photo-induced spin wave tunneling through an air gap
  invited: true
  authors: T. Satoh
date: 2017.08.07 18:30:00
categories: colloquiums
---
