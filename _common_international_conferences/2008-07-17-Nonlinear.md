---
layout: ja/default
meeting:
  title: International Workshop on "Nonlinear wave-mixing for laser technology"
  place: Chiba, Japan
  period: 17-18
first_presentation:
  title: Mid-infrared generation in DAST
  authors: K. Kuroda, Y. Toya, S. Yamamoto, T. Satoh, T. Shimura, S. Ashihara, Y. Takahashi, M. Yoshimura, Y. Mori, and T. Sasaki
date: 2008.07.17.10:00
categories: international_conference
---
