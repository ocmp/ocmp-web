---
layout: ja/default
meeting:
  title: AIST-RIKEN Joint WS
  place: Okinawa, Japan
  period: 4-7
first_presentation:
  id: P22
  title: Photo-induced Faraday effect in nickel oxide
  authors: T. Satoh, S. J. Cho, T. Shimura, K. Kuroda, H. Ueda, and Y. Ueda
date: 2009.03.04.10:00
categories: international_conference
---
