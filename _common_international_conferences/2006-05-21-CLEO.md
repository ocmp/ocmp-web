---
layout: ja/default
meeting:
  title: Conference on Lasers and Electro-Optics (CLEO) 2006 and Quantum Electronics and Laser Science Conference (QELS) 2006
  place: Long Beach, California, United States
  period: 21-26
first_presentation:
  title: Ultrafast manipulation of antiferromagnetism in NiO
  authors: M. Fiebig, N. P. Duong, and T. Satoh
  
date: 2006.05.21 10:00:00
categories: colloquiums
---
