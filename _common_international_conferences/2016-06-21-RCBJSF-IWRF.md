---
layout: ja/default
meeting:
  title: 13th Russia/CIS/Baltic/Japan Symposium on Ferroelectricity (RCBJSF)-International Workshop on Relaxer Ferroelectrics (IWRF)
  place: Matsue, Japan
  period: 19-23
first_presentation:
  id: Invite18
  title: Writing and reading of an optical polarization state in hexagonal YMnO<sub>3</sub>
  invited: true
  authors: T. Satoh
date: 2016.06.21.10:00
categories: international_conference
---
