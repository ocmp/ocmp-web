---
layout: ja/default
meeting:
  title: International Conference on Solid State Devices and Materials (SSDM) 2013
  place: Fukuoka, Japan
  period: 24-27
first_presentation:
  id: F-1-1
  title: Generation and Directional Control of Spin Wave by Spatially-Shaped Light Pulses
  invited: true
  authors: T. Satoh
date: 2013.09.24.10:00
categories: international_conference
---
