---
layout: ja/default
meeting:
  title: CEMS Symposium on Trends in Condensed Matter Physics
  place: RIKEN, Saitama, Japan
  period: 6-8
first_presentation:
  title: Optical excitation of magnons in antiferromagnets
  authors: T. Satoh
  invited: true
  date: 2017.11.06 18:30:00
categories: colloquiums
---
