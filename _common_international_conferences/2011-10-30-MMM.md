---
layout: ja/default
meeting:
  title: 56th Conference on "Magnetism &amp; Magnetic Materials"
  place: Scottsdale, Arizona, USA
  period: 30 - Nov. 3
first_presentation:
  id: EQ-12
  title: Two-Dimensional Propagation of the Spin Wave Packet Excited by the Inverse Faraday Effect
  authors: Y. Terui, T. Satoh, T. Shimura, K. Kuroda, R. Moriya, K. Ando, and E. Saitoh
date: 2011.10.30.10:00
categories: international_conference
---
