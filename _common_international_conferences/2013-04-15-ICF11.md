---
layout: ja/default
meeting:
  title: The 11th International Conference on Ferrites (ICF 11)
  place: Okinawa, Japan
  period: 15-19
first_presentation:
  id: 18pA1-I-4
  title: Opto-magnonics&#x3a; Spin-wave manipulation by light pulses in garnet ferrite crystals
  invited: true
  authors: T. Satoh, Y. Terui, R. Moriya, B. A. Ivanov, K. Ando, E. Saitoh, T. Shimura, and K. Kuroda
date: 2013.04.15.10:00
categories: international_conference
---
