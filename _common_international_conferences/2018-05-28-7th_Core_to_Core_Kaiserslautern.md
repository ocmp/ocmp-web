---
layout: ja/default
meeting:
  title: 7th Workshop of the Core-to-Core Project Tohoku-York-Kaiserslautern
  place: Kaiserslautern, Germany
  period: 28-30
first_presentation:
  title: Ultrafast excitation of coherent phonons in garnet
  id: P6
  authors: M. Kanamaru, P. Khan, W. H. Hsu, M. Kichise, Y. Fujii, A. Koreeda, and T. Satoh
second_presentation:
  title: Observation of THz coherent phonon modes in multiferroic BiFeO<sub>3</sub> by femtosecond laser pulses
  id: P7
  authors: P. Khan, M. Kanamaru, A. Haizuka, T. Ito, and T. Satoh
third_presentation:
  title: Polarized Raman scattering study of multiferroic BiFeO<sub>3</sub> single crystal
  id: P8
  authors: M. Kichise, W. H. Hsu ,T. Ito, and T. Satoh
fourth_presentation:
  title: Brillouin light scattering probing exchange-dominated spin wave
  id: P12
  authors: K. Matsumoto, T. Br&auml;cher, P. Pirro, T. Fischer, D. Bozhko, M. Geilen, F. Heussner, T. Meyer, B. Hillebrands, and T. Satoh
date: 2018.05.28 18:30:00
categories: colloquiums
---
