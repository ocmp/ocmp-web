---
layout: ja/default
meeting:
  title: JSPS-EPSRC-DFG Core-to-Core Programme York-Tohoku-Kaiserslautern Research Symposium on “New-Concept Spintronics Devices”
  place: York, UK
  period: 12-14
first_presentation:
  title: Magnon and phonon Raman scattering in Y<sub>3</sub>Fe<sub>5</sub>O<sub>12</sub>
  id: Poster-02
  authors: W.-H. Hsu, M. Kichise, Y. Fujii, A. Koreeda, and T. Satoh
second_presentation:
  title: Angle-resolved polarized Raman spectroscopy of magnons in BiFeO<sub>3</sub>
  id: Poster-04
  authors: M. Kichise, Y. Fujii, A. Koreeda, T. Ito, and T. Satoh
third_presentation:
  title: Coherent magnon and phonon-polariton excited by ultrashort pulse laser
  id: Poster-09
  authors: K. Matsumoto, P. Khan, M. Kanamaru, T. Ito, and T. Satoh
fourth_presentation:
  invited: true
  title: Magnon and phonon-polariton excitations in multiferroic BiFeO<sub>3</sub>
  authors: T. Satoh
date: 2019.06.12 18:30:00
categories: colloquiums
---
