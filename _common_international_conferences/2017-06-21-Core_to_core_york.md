---
layout: ja/default
meeting:
  title: York-Tohoku-Kaiserslautern Research Symposium on "New-Concept Spintronics Devices"
  place: The University of York, UK
  period: 21-23
first_presentation:
  title: Transmission of photoinduced spin wave through an air gap
  authors: K. Matsumoto, I. Yoshimine, K. Himeno, and T. Satoh
second_presentation:
  title: Femtosecond laser induced THz and GHz oscillation in iron garnet
  authors: M. Kanamaru, P. Khan, and T. Satoh
date: 2017.06.07 18:30:00
categories: colloquiums
---
