---
layout: ja/default
meeting:
  title: Tokyo Institute of Technology and Stony Brook University Joint Science and Technology Meeting
  place: Tokyo, Japan
  period: 22-23
first_presentation:
  invited: true
  title: Optomagnonics in multiferroic BiFeO<sub>3</sub>
  authors: T. Satoh
date: 2019.05.22 18:30:00
categories: colloquiums
---
