---
layout: ja/default
meeting:
  title: Magnonics 2013
  place: Varberg, Sweden
  period: 4-8
first_presentation:
  title: Spin wave emission by circularly and linearly polarized light pulses in garnet ferrite crystals
  authors: T. Satoh, I. Yoshimine, Y. Terui, R. Moriya, B. A. Ivanov, K. Ando, E. Saitoh, T. Shimura, and K. Kuroda
date: 2013.08.04.10:00
categories: international_conference
---
