---
layout: ja/default
meeting:
  title: Topological lightwave synthesis and its applications 2012
  place: Chiba, Japan
  period: 5-6
first_presentation:
  id: 5P-3
  title: Spin wave synthesis by spatially shaped light pulses
  invited: true
  authors: T. Satoh
date: 2012.07.05.10:00
categories: international_conference
---
