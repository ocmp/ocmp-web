---
layout: ja/default
meeting:
  title: 2007 CERC International Symposium
  place: Tokyo, Japan
  period: 22-25
first_presentation:
  id: 1P58
  title: Dynamic processes of antiferromagnetic compounds probed by second-harmonic generation
  authors: T. Satoh, N. P. Duong, and M. Fiebig
date: 2007.05.22.10:00
categories: international_conference
---
