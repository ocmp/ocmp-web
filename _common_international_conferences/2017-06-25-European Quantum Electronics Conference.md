---
layout: ja/default
meeting:
  title: European Quantum Electronics Conference (EQEC)
  place: Munich, Germany
  period: 25-29
first_presentation:
  id: EG_P_18
  title: Surface-plasmon enabled control over magnetization dynamics in hybrid magnetoplasmonic crystals
  authors: A. L. Chekhov, I. Razdolski, A. I. Stognij, T. Satoh, T. V. Murzina, and A. Stupakiewicz
date: 2017.06.25 18:30:00
categories: colloquiums
---
