---
layout: ja/default
meeting:
  title: CEMS Symposium on Quantum Information and Spintronics
  place: Tokyo, Japan
  period: 10-12
first_presentation:
  title: True chirality and angular momentum of phonons in α-quartz
  id: PS1-1
  authors: Y.-C. Huang, G. Kusuno, H. Kusunose, T. Satoh
second_presentation:
  title: Circularly polarized Raman scattering in ferroaxial NiTiO<sub>3</sub>
  id: PS2-3
  authors: G. Kusuno, T. Hayashida, T. Nagai, H. Watanabe, T. Kimura, T. Satoh
date: 2024.12.10 18:30:00
categories: colloquiums
---

