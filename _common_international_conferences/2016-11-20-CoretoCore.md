---
layout: ja/default
meeting:
  title: 4th JSPS Core-to-Core Workshop on "New-Concept Spintronic Devices"
  place: Tohoku University, Japan
  period: 19-20
first_presentation:
  id:
  title: Time- and space-resolved spin wave transmission through an air gap
  invited: true
  authors: T. Satoh
date: 2016.11.20.10:00
categories: international_conference
---
