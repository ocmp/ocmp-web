---
layout: ja/default
meeting:
  title: 1st KAI-X Global Conference in Physics-International workshop on Orbitronics (2023 Orbitronics workshop)
  place: KAIST, Daejeon, Republic of Korea
  period: 22
first_presentation:
  title : Truly chiral phonons in chiral materials α-HgS and Te <b>(invited)</b>
  authors: T. Satoh
date: 2023.11.22 18:30:00
categories: colloquiums
---

