---
layout: ja/default
meeting:
  title: International School on Spintronics and Spin-Orbitronics
  place: Fukuoka, Japan
  period: 16-17
first_presentation:
  id: P-62
  title: Detection of exchange resonance mode in rare-earth iron garnet
  authors: P. Khan, K. Himeno, K. Matsumoto, M. Kanamaru, and T. Satoh
second_presentation:
  id: P-63
  title: Magnon Raman spectroscopy in antiferromagnetic CoO
  authors: K. Tsuchida, Y. Fujii, A. Koreeda, and T. Satoh
date: 2016.12.17 18:30:00
categories: colloquiums
---
