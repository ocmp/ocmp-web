---
layout: ja/default
meeting:
  title: Korea-Japan Spin-Orbit Workshop
  place: Fukuoka, Japan
  period: 15-16
first_presentation:
  id:
  title: Optical excitation of coupled spin-orbit dynamics in antiferromagnetic CoO
  invited: true
  authors: T. Satoh
date: 2016.12.16.11:55
categories: international_conference
---
