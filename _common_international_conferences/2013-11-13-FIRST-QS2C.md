---
layout: ja/default
meeting:
  title: FIRST-QS2C Workshop on "Emergent Phenomena of Correlated Materials"
  place: Shinagawa Intercity Hall, Japan
  period: 13-16
first_presentation:
  id: P-58
  title: Optical excitation of spin oscillations in NiO
  authors: T. Satoh, K. Otani, S.-J. Cho, R. Iida, T. Shimura, K. Kuroda, H. Ueda, Y. Ueda, B. A. Ivanov, F. Nori, and M. Fiebig
date: 2013.11.13.10:00
categories: international_conference
---
