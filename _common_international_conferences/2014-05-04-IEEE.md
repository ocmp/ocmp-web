---
layout: ja/default
meeting:
  title: IEEE International Magnetics Conference, INTERMAG Europe 2014
  place: Dresden, Germany
  period: 4-8
first_presentation:
  id: "&#035;9 BV"
  title: Magnetization reversal and magnetic domain structures in Gd-Yb-BIG crystals
  authors: S. Parchenko, M. Tekielak, I. Yoshimine, T. Satoh, A. Maziewski, and A. Stupakiewicz
date: 2014.05.04.10:00
categories: international_conference
---
