---
layout: ja/default
meeting:
  title: CLEO&#58;  QELS-Fundamental Science
  place: San Jose, California, United States
  period: 5-10
first_presentation:
  title: Two-dimensional THz spectroscopy of exchange interactions in rare-earth doped garnets
  id: FM3D.3
  authors: S. Pal, C. Tzschaschel, A. Bortis, T. Satoh, and M. Fiebig
date: 2019.05.05 18:30:00
categories: colloquiums
---
