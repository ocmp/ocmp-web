---
layout: ja/default
meeting:
  title: 55th Conference on "Magnetism &amp; Magnetic Materials"
  place: Atlanta, Georgia, USA
  period: 14-18
first_presentation:
  id: AP-12
  title: Coherent spin precession induced by circularly and linearly polarized light pulses in DyFeO<sub>3</sub>
  authors: R. Iida, T. Satoh, T. Shimura, K. Kuroda, Y. Tokunaga, and Y. Tokura
date: 2010.11.14.10:00
categories: international_conference
---
