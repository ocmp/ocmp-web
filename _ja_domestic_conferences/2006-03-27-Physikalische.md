---
layout: ja/default
meeting:
  title: Deutsche Physikalische Gesellschaft e. V. (DPG)
  place: Dresden, Germany
  period: 27-31
first_presentation:
  id: MA 11.6
  title: Domain imaging and symmetry reduction in LiNiPO<sub>4</sub>
  authors: B. B. van Aken, T. Satoh, and M. Fiebig
second_presentation:
  id: MA 20.164
  title: Ultrafast spin and lattice dynamics in antiferromagnetic Cr<sub>2</sub>O<sub>3</sub>
  authors: B. B. van Aken, T. Satoh, N. P. Duong, and M. Fiebig
date: 2006.03.27.10:00
categories: domestic_conference
---
