---
layout: ja/default
meeting:
  title:  DPG Spring Meeting of the Condensed Matter Section (SKM) together with the EPS
  place:  Berlin, Germany
  period: 11-14
first_presentation:
  id: MA 6.2
  title: Optically induced symmetry breaking in multiferroic h-YMnO<sub>3</sub> probed by second harmonic generation
  authors: C. Tzschaschel, M. Weber, M. Fiebig, and T. Satoh
second_presentation:
  id: MA 49.1
  title: Exploring the exchange interactions in GdYb-BIG via two-dimensional THz spectroscopy
  authors: S. Pal, C. Tzschaschel, T. Satoh, and M. Fiebig
date: 2018.03.11.10:00
categories: domestic_conference
---
