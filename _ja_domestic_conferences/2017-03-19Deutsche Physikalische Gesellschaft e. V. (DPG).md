---
layout: ja/default
meeting:
  title: Deutsche Physikalische Gesellschaft e. V. (DPG)
  place: Dresden, Germany
  period: 19-24
first_presentation:
  id: MA 23.3
  title: Ultrafast excitation of coherent magnons by circularly and linearly polarized light pulses in antiferromagnetic NiO
  authors: C. Tzschaschel, K. Otani, R. Iida, T. Shimura, H. Ueda, S. G&uuml;nther, M. Fiebig, and T. Satoh
date: 2017.03.19 18:30:00
categories: colloquiums
---
