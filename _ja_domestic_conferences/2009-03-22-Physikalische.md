---
layout: ja/default
meeting:
  title: Deutsche Physikalische Gesellschaft e. V. (DPG)
  place: Dresden, Germany
  period: 22-27
first_presentation:
  id: MA 35.14
  title: Three-dimensional control of the antiferromagnetic order parameter in nickel oxide
  authors: A. Rubano, T. Satoh,  and M. Fiebig
date: 2009.03.22.10:00
categories: domestic_conference
---
