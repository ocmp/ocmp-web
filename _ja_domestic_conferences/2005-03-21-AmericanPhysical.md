---
layout: ja/default
meeting:
  title: American Physical Society (APS) March Meeting 2005
  place: Los Angeles, CA, USA
  period: 21-25
first_presentation:
  id: V42.00002
  title: Ultrafast magnetization Dynamics of Antiferromagntic NiO
  authors: M. Fiebig, N. P. Duong, and T. Satoh
date: 2005.03.21.10:00
categories: domestic_conference
---
