---
layout: ja/default
meeting:
  title: 第39回光学シンポジウム
  place: 東京大学生産技術研究所
  period: 26-27
first_presentation:
  id: 13
  title: 超短光パルスによるスピン波励起とその伝播の時空間イメージング
  authors: 吉峯功, 佐藤琢哉, 志村努
date: 2014.06.26
categories: domestic_conference
---
