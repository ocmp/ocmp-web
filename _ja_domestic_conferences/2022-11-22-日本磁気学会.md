---
layout: ja/default
meeting:
  title: 日本磁気学会 第239回研究会／第46回光機能磁性デバイス・材料専門研究会「光による磁気物性計測と光計測技術の最前線」
  place: オンライン
  period: 22

first_presentation:
  title: 反強磁性体の超高速磁化ダイナミクス測定 <b>(invited)</b>
  authors: 佐藤琢哉

date: 2022.11.22 11:00:00
categories: colloquiums
---

