---
layout: ja/default
meeting:
  title: NCCR MUST Annual Meeting
  place: Grindelwald, Switzerland
  period: 14-16
first_presentation:
  title: Tracking the order parameter motion during a coherent antiferromagnetic spin precession
  authors: C. Tzschaschel, T. Satoh, and M. Fiebig
date: 2019.01.14 17:00:00
categories: colloquiums
---
