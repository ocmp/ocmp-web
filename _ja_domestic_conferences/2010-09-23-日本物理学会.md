---
layout: ja/default
meeting:
  title: 日本物理学会「2010年秋季大会」
  place: 大阪府立大学
  period: 23-26
first_presentation:
  id: 25pPSB-34
  title: DyFeO<sub>3</sub>における近赤外光パルスで誘起したテラヘルツ振動モード
  authors: 飯田隆吾，佐藤琢哉，志村努，黒田和男，徳永祐介，十倉好紀
second_presentation:
  id: 25pPSB-44
  title: 反強磁性体のTHz吸収分光
  authors: 森圭輔，飯田隆吾，佐藤琢哉，志村努，黒田和男
date: 2010.09.23.10:00
categories: domestic_conference
---
