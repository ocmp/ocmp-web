---
layout: ja/default
meeting:
  title: 日本物理学会 「第79回年次大会」
  place: 北海道大学
  period: 16-19

first_presentation:
  id: 16aE308-7
  title: カイラル結晶中のカイラルフォノンと角運動量 <b>(invited)</b>
  authors: 佐藤琢哉

second_presentation:
  id: 17aPS-80
  title: フェロアキシャル物質NiTiO<sub>3</sub>の円偏光ラマン分光
  authors: 楠野楽到, 林田健志, 永井隆之, 木村剛, 佐藤琢哉


date: 2024.09.16 11:00:00
categories: colloquiums
---
