---
layout: ja/default
meeting:
  title: 日本物理学会「第55回年次大会」
  place: 新潟大学
  period: 22-25
first_presentation:
  id: 23pTF-13
  title: 電荷密度波物質K<sub>0.3</sub>MoO<sub>3</sub>における光励起効果
  authors: 小川直毅，白神彰則，近藤隆祐，鹿児島誠一，佐藤琢哉，宮野健次郎
date: 2000.09.22
categories: domestic_conference
---
