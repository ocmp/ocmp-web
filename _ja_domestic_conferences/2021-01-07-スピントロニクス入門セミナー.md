---
layout: ja/default
meeting:
  title: 第19回 スピントロニクス入門セミナー
  place: オンライン
  period: 7-8

first_presentation:
  title: 光マグノニクス <b>(invited)</b>
  authors: 佐藤琢哉

date: 2021.1.7 11:00:00
categories: colloquiums
---
