---
layout: ja/default
meeting:
  title: 九州大学テクノロジーフォーラム2014
  place: 東京国際フォーラム
first_presentation:
  id: G410-07
  title: 次世代スピントロニクスに向けた時間分解スピン波イメージングの手法開発
  authors: 佐藤琢哉
date: 2014.12.03.09:00
categories: domestic_conference
---
