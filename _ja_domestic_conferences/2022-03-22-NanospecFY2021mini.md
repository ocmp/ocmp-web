---
layout: ja/default
meeting:
  title: SPring-8ユーザー協同体顕微ナノ材料科学研究会, 日本表面真空学会放射光表面科学研究部会, 日本表面真空学会プローブ顕微鏡研究部会 合同ミニコンファレンス NanospecFY2021mini
  place: オンライン
  period: 22

first_presentation:
  title: 走査型磁気光学Kerr顕微鏡によるカイラル反強磁性体Mn<sub>3</sub>Irの磁気ドメインの可視化
  authors: 山口湖太郎, 山田貴大, 小林裕太, 小野輝男, 森山貴広, 佐藤琢哉 

date: 2022.03.22 11:00:00
categories: colloquiums
---






