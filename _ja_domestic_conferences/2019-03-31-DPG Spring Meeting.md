---
layout: ja/default
meeting:
  title: DPG Spring Meeting
  place: Regensburg, Germany
  period: 31-Apr. 5
first_presentation:
  title: Tracking the order parameter motion during a coherent antiferromagnetic spin precession
  id: MA 26.7
  authors: C. Tzschaschel, T. Satoh, and M. Fiebig
  date: 2019.03.31 18:30:00
categories: colloquiums
---
