---
layout: ja/default
meeting:
  title:  日本物理学会「第72回年次大会」
  place:  大阪大学豊中キャンパス
  period: 17-20
first_presentation:
  id: 19pK-PS-18
  title: 反強磁性体CoOのマグノン・顕微ラマン分光測定
  authors: 土田孝三, 藤井康裕, 是枝聡肇, 佐藤琢哉
date: 2017.03.19.13:00
categories: domestic_conference
---
