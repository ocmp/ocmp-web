---
layout: ja/default
meeting:
  title: DPG Meeting of the Condensed Matter Section (SKM)
  place: Online
  period: 27-Oct. 1
first_presentation:
  id: MA 15.9
  title: Efficient spin excitation via ultrafast damping torques in antiferromagnets
  authors: C. Tzschaschel, T. Satoh, and M. Fiebig


date: 2021.09.27 17:00:00
categories: colloquiums
---



