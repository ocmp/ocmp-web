---
layout: ja/default
meeting:
  title: 2012年春季 第59回応用物理学関係連合講演会
  place: 早稲田大学
  period: 15-18
first_presentation:
  id: 15p-GP4-7
  title: パルス光励起によるGaPの光ガルバノ効果と光電流
  authors: 吉峯功，藤村隆史，佐藤琢哉，Alexei A. Kamshilin, 志村努，黒田和男
second_presentation:
  id: 17p-B4-17
  title: 円偏光パルスの成形によるスピン波の波数分布制御
  authors: 照井勇輝，佐藤琢哉，守谷頼，志村努，黒田和男
date: 2012.03.15.10:00
categories: domestic_conference
---
