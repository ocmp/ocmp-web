---
layout: ja/default
meeting:
  title: Deutsche Physikalische Gesellschaft e. V. (DPG)
  place: Berlin, Germany
  period: 4-9
first_presentation:
  id: MA 6.2
  title: Ultraschnelle Magnetisierungsdynamik von Antiferromagneten
  authors: M. Fiebig, N. P. Duong, and T. Satoh
second_presentation:
  id: MA 8.2
  title: Phase-sensitive Ultrafast Spin Dynamics in Antiferromagnetic Cr<sub>2</sub>O<sub>3</sub>
  authors: T. Satoh, B. B. van Aken, N. P. Duong, Th. Lottermoser, and M. Fiebig
third_presentation:
  id: MA 15.10
  title: Detection of Spin and Charge States in Centrosymmetric Materials by Nonlinear Optics
  authors: T. Satoh, Th. Lottermoser, M. Fiebig, Y. Ogimoto, H. Tamaru, M. Izumi, K. Miyano, and S. Ishihara
date: 2005.03.04.10:00
categories: domestic_conference
---
