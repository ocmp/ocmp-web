---
layout: ja/default
meeting:
  title: 日本物理学会 「2021年秋季大会」
  place: オンライン
  period: 20-23
first_presentation:
  id: 21aK1-6
  title: 共焦点顕微ラマン分光によるビスマスフェライトの磁気ドメインイメージング
  authors: 藤井康裕, 大石栄一, 是枝聡肇, 吉瀬みのり, 佐藤琢哉, 伊藤利充

second_presentation:
  id: 21aK1-7
  title: BiFeO<sub>3</sub>におけるマグノンの円偏光ラマンスペクトルの温度依存性 
  authors: 大石栄一, 藤井康裕, 是枝聡肇, 佐藤琢哉, 伊藤利充

third_presentation:
  id: 21aK1-8
  title: マルチフェロイック物質BiFeO<sub>3</sub>の準弾性光散乱
  authors: 大石栄一, 藤井康裕, 是枝聡肇, 佐藤琢哉, 伊藤利充

fourth_presentation:
  id: 23aC1-7
  title: フェリ磁性体の超高速光マグノニクス (invited)
  authors: 佐藤琢哉

date: 2021.09.20 17:00:00
categories: colloquiums
---
