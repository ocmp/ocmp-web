---
layout: ja/default
meeting:
  title: 電気情報通信学会 磁気記録・情報ストレージ研究会
  place: 九州大学西新プラザ
  period: 20-21
first_presentation:
  id: 
  title: 光誘起後進体積静磁波，交換共鳴モードの時間分解イメージング
  authors: 松本慧大, 姫野滉盛, 吉峯功, 佐藤琢哉
date: 2016.10.20.10:00
categories: domestic_conference
---