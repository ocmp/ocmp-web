---
layout: ja/default
meeting:
  title: 九州大学量子ナノスピン物性研究センター研究会 「ナノ物理研究の最前線」
  place: 九州大学伊都キャンパス
first_presentation:
  id: 
  title: フェムト秒光パルスによる超高速ピン波制御
  authors: 佐藤琢哉
date: 2015.02.06.10:00
categories: domestic_conference
---
