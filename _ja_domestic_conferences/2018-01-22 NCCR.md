---
layout: ja/default
meeting:
  title:  8th NCCR MUST Annual Meeting
  place:  Grindelwald, Switzerland
  period: 22-24
first_presentation:
  id: 68
  title: Effect of Gilbert damping on ultrafast optical spin excitations in antiferromagnetic hexagonal HoMnO<sub>3</sub>
  authors: C. Tzschaschel, M. Weber, M. Fiebig, and T. Satoh
date: 2018.01.22.10:00
categories: domestic_conference
---
