---
layout: ja/default
meeting:
  title: Deutsche Physikalische Gesellschaft e. V. (DPG)
  place: Regensburg, Germany
  period: 8-12
first_presentation:
  id: MA 13.124
  title: Resonant Two-Photon Sum Frequency Generation on Centrosymmetric NiO and KNiF<sub>3</sub>
  authors: T. Satoh, Th. Lottermoser, and M. Fiebig
second_presentation:
  id: MA 13.67
  title: Ultrafast Magnetization Dynamics in Antiferromagnets
  authors: N. P. Duong, T. Satoh, Th. Lottermoser, and M. Fiebig
date: 2004.03.12.10:00
categories: domestic_conference
---
