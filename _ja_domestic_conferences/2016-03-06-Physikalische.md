---
layout: ja/default
meeting:
  title: Deutsche Physikalische Gesellschaft e. V. (DPG)
  place: Regensburg, Germany
  period: 6-11
first_presentation:
  id: MA 13.5
  title: Determining the Verdet constant in antiferromagnetic materials
  authors: C. Tzschaschel, S. G&uuml;nther, T. Satoh, and M. Fiebig
date: 2016.03.06.10:00
categories: domestic_conference
---