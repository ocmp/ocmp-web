---
layout: ja/default
meeting:
  title: 日本磁気学会第170回研究会 第4回光機能磁性デバイス・材料専門研究会共催, 「光と磁気のシナジー技術」 ～次世代ストレージ・光機能磁性デバイス実現のための新技術動向～
  place: Tokyo, Japan
first_presentation:
  title: 円偏光パルスで誘起された反強磁性体のスピン歳差運動
  invited: true
  authors: 佐藤琢哉，飯田隆吾，志村努，黒田和男，植田浩明，上田寛
date: 2010.01.29.10:00
categories: domestic_conference
---
