---
layout: ja/default
meeting:
  title: 第40回 強誘電体会議
  place: 京都工芸繊維大学, オンライン
  period: 24-27

first_presentation:
  id: 26am-01
  title: マルチフェロイック物質BiFeO<sub>3</sub>のブリルアン散乱
  authors: 石田啓, 大石栄一, 藤井康裕, 是枝聡肇, 伊藤利充, 佐藤琢哉

date: 2023.05.01 11:00:00
categories: colloquiums
---


