---
layout: ja/default
meeting:
  title: 第37回 強誘電体会議
  place: コープイン京都, 現地開催中止
  period: 27-30

first_presentation:
  id: 27pm-09
  title: LiNbO<sub>3</sub>におけるフォノン-ポラリトンの時空間分解測定と電場解析
  authors: 松本慧大, 佐藤琢哉

date: 2020.05.27 11:00:00
categories: colloquiums
---
