---
layout: ja/default
meeting:
  title: Deutsche Physikalische Gesellschaft e. V. (DPG)
  place: Berlin, Germany
  period: 25-29
first_presentation:
  id: MA 20.8
  title: Spindynamics of multiferroic HoMnO<sub>3</sub>
  authors: Tim Hoffmann, Tim G&uuml;nter, T. Satoh, T. Lottermoser, and M. Fiebig
second_presentation:
  id: MA 32.73
  title: Investigation of multiferroics with ultrafast nonlinear optics
  authors: Tim G&uuml;nter, Tim Hoffmann, T. Satoh, T. Lottermoser, and M. Fiebig
date: 2008.02.25.10:00
categories: domestic_conference
---
