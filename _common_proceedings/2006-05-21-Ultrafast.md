---
layout: ja/default
title: Ultrafast manipulation of antiferromagnetism in NiO
authors: M. Fiebig, N. P. Duong, and T. Satoh
body: <a target="_blank"  href="https://doi.org/10.1109/CLEO.2006.4629114">Proceedings of CLEO/QELS06, QTuH3</a> (21-26 May 2006, Long Beach, USA).
date: 2006.05.21.10:00
categories: proceeding
---
