---
layout: ja/default
title: Ultrafast processes in antiferromagnetic Cr<sub>2</sub>O<sub>3</sub>
authors: N. P. Duong, T. Satoh, and M. Fiebig
body: Proceedings of 9th Asia Pacific Physics Conference (9th APPC), p. 277-278 (Oct. 25-31, 2004, Hanoi, Vietnam).
date: 2004.10.25.09:00
categories: proceeding
---
