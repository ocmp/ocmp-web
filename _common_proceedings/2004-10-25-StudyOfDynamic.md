---
layout: ja/default
title: Study of dynamic processes in antiferromagnetic hexagonal manganites YMnO<sub>3</sub> and HoMnO<sub>3</sub> by second harmonic generation
authors: N. P. Duong, T. Satoh, and M. Fiebig
body: Proceedings of 9th Asia Pacific Physics Conference (9th APPC), p. 277-278 (Oct. 25-31, 2004, Hanoi, Vietnam).
date: 2004.10.25.10:00
categories: proceeding
---
