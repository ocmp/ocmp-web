---
layout: en/page
title: Contact
global_navi_ja: false
global_navi_en: true
permalink: /en/contact/
---

We are looking for Master’s and PhD students who belong to Department of Physics, School of Science, Tokyo Institute of Technology (Institute of Science Tokyo from October 2024). Information on an entrance examination is available [here](https://www.titech.ac.jp/english/graduate_school/international/graduate_program_c/). 
There are two entrances to the Master or PhD course, Institute of Science Tokyo.

Concerning the financial support, we usually recommend [the Japanese Government (Monbukagakusho) Scholarship Students](https://www.titech.ac.jp/english/graduate_school/international/?fbclid=IwAR2xxVwOqeMKiF8aYBI_em_4psKCQ0_-rz9UdIbvaT4rwXKIo0j70Cpwrfs). The application deadline is April 2025, the earliest entrance is April 2026. Talented postdoctoral researchers are welcome to our laboratory. Please apply to [the JSPS fellowship](http://www.jsps.go.jp/english/e-fellow/index.html). 
If you are interested, please contact Takuya Satoh (Professor) for further information.

{:refdef .contact-table}
|**Contact**        |Takuya Satoh (Professor)|
|**Affiliation**    |Satoh Laboratory, Department of Physics, Institute of Science Tokyo|
|**Address**    |2-12-1 (S5-3), Ookayama, Meguro-ku, Tokyo 152-8551, Japan|
|**Tel**        |+81-3-5734-2716|
|**E-mail**     |![satoh](/assets/images/email/satoh-mail.png){: .email-image}|
{: refdef}

## Access

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d482.22547513429294!2d139.68413290597783!3d35.602736353573825!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f5312c64657f%3A0xc5d613e39931acf1!2sSouth+Building+5%2C+1-ch%C5%8Dme-31+Ishikawach%C5%8D%2C+Ota+City%2C+T%C5%8Dky%C5%8D-to+145-0061!5e0!3m2!1sen!2sjp!4v1554266776448!5m2!1sen!2sjp" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
