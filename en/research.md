---
layout: en/page
title: Research
global_navi_ja: false
global_navi_en: true
permalink: /en/research/
---

{:refdef .page-sub-menu}
- {:refdef .page-sub-menu-list .col-x-4}[Antiferromagnetic terahertz magnonics](#terahertz)
- {:refdef .page-sub-menu-list .col-x-4}[Opto-magnonics](#opto-magnonics)
- {:refdef .page-sub-menu-list .col-x-4}[Opto-spintronics](#opto-spintronics)
- {:refdef .page-sub-menu-list .col-x-4}[Opto-chiral phononics](#opto-chiralphononics)
{: refdef}


{:refdef .content-heading #terahertz}
## Antiferromagnetic terahertz magnonics
{: refdef}

Light has been used to heat up magnetic media in magneto-optical recording, such as MO disks and next-generation magneto-optical hybrid recording (Thermally-Assisted Magnetic Recording). In this mode, the recording speed and density are limited by the cooling rate and the thermal diffusion time. Spin manipulation is achieved mainly by using an external magnetic field, and light plays the minor role of assisting the spin manipulation at lower magnetic fields by reducing the coercivity of the magnetic media by heating it. We will study ultrafast spin manipulation by using mainly light, without a magnetic field (or with the help of a magnetic field). Light pulses will be temporally shaped to achieve the most efficient spin manipulation.

Ferromagnetic materials have conventionally been used as magnetic recording media. The spin reversal speed is proportional to the precession frequency, which is limited to GHz order. Instead, We will focus on antiferromagnets, which possess the highest frequency (~THz). The combination of temporally shaped light pulses and antiferromagnetic materials will enable us to demonstrate fast and arbitrary spin manipulation.

{:refdef .content-heading #opto-magnonics}
## Opto-magnonics
{: refdef}

In electronics, which uses the charge degree-of-freedom of the electron, one of the most serious problems is the increase in Joule heating associated with miniaturization and integration. In the technology of spintronics, which exploits the spin degree-of-freedom of the electron, Joule heating by free electrons in the form of spin current in a metal is still not completely eliminated. On the other hand, using spin waves as a spin current in an insulator overcomes this problem, and this approach has been studied extensively in the field of "magnonics". In spite of its importance, this field had attracted little interest in Japan until very recently. Furthermore, the generation, manipulation, and detection of spin waves have been achieved by electromagnetic induction using an antenna or spin-transfer torque using a spin-polarized current. These methods require microfabrication, which offers limited manipulation freedom after fabrication. Therefore, arbitrary tuning of spin wave propagation without the use of microfabrication is of prime importance.

We have proposed a nonthermal spin wave generation technique that utilizes a circularly polarized light pulse via the inverse Faraday effect. We have also proposed a technique for manipulating the direction of spin wave propagation by shaping a light spot at the sample surface. This will be the first step in the new field of "opto-magnonics" (T. Satoh et al., Nature Photonics (2012)).
The final goal is to pioneer a new field of "opto-magnonics" by exploiting ultrashort-light pulses and to realize ultrafast opto-magnetic switching devices.

{:refdef .content-heading #opto-spintronics}
## Opto-spintronics
{: refdef}

Spintronics, which uses the charge as well as spin degrees of freedom of electrons, has made remarkable progress in recent years.  In Spintronics, it has been intensively investigated to develop technologies for generating spin current, which is the flow of spin angular momentum of electrons, from electric current and heat current. On the other hand, the irradiation of light also enables us to generate spin currents. We have been studying the optical generation of spin currents and spin-polarized states in magnetic materials. Furthermore, by using the spin currents and spin-polarized states, we aim to create novel opto-magnetic devices which can operate at ultrafast speeds with light alone.

{:refdef .content-heading #opto-chiralphononics}
## Opto-chiral phononics
{: refdef}

Chirality is a form of asymmetry inherent in nature, observed at all levels, and has garnered attention in various fields of research. Our laboratory is conducting research on chiral phonons that rotate while propagating in three-dimensional chiral materials, using circularly polarized Raman scattering and first-principles calculations. This research is expected to contribute to the development of new methods for transferring angular momentum from photons to electron spins through propagating chiral phonons.