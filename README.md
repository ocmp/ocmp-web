# 九州大学光物性研究室のWEBサイト


## Clone

次のコマンドでローカルにダウンロードしてください。

```shell
git clone git@bitbucket.org:ocmp/ocmp-web.git
```

## Setup

Rubyのインストールが必要です。ターミナルで以下のコマンドを実行して、バージョン情報が返ってくるか確かめてください。帰ってこない場合はインストールしておいてください。

```
ruby -v
```

Rubyが入っているようでしたら、次のコマンドでBundleをインストールしてください。

```
gem install bundle
```

次に、ocmp-web のディレクトリ内で、以下のコマンドを入力してください。

```
make setup
```

ここまでで、必要なパッケージ類のインストールが完了しました。

以下のコマンドでWEBサーバーをローカルに起動して、変更を確認しながら編集していきましょう。

```
make serve
```

## Authors

K.Himeno
