---
layout: ja/page_member
title: 岩瀬篤広
name:
  ja:
    first: 篤広
    last: 岩瀬
    ruby:
      first: あつひろ
      last: いわせ
  en:
    short: A. Iwase
    first: Atsuhiro
    last: Iwase
email: 
image: /assets/images/member/2022/iwase.jpg
position: Master
---

<!-- ## メールアドレス
![iwase](/assets/images/email/iwase-mail.png){: .email-image} -->


<!-- ## 研究テーマ


## 出身地 -->


## 一言
ロッテファンです