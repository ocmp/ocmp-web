---
layout: ja/page_member
title: 茆慧玲
name:
  ja:
    first: 慧玲
    last: 茆
    ruby:
      first: フアリン
      last: マオ
  en:
    short: H. Mao
    first: Huiling
    last: Mao
image: /assets/images/member/2019/mao.jpg
position: Ph.D
---

## Email
![mao](/assets/images/email/mao_mail.png){: .email-image}

## 研究テーマ



## 出身地

中国 安徽省

## 一言

Do one thing at a time, and do well. I really enjoy eating local food and visiting different places.
