---
layout: ja/page_member
title: 水野 昌人
name:
  ja:
    first: 昌人
    last: 水野
    ruby:
      first: まさと
      last: みずの
  en:
    short: M. Mizuno
    first: Masato
    last: Mizuno
email: 
image: /assets/images/member/2021/Mizuno.jpg
position: Master
---

<!-- ## メールアドレス
![mizuno](/assets/images/email/mizuno-mail.png){: .email-image} -->


## 研究テーマ


## 出身地
岐阜県

## 一言
お菓子作りにはまってます