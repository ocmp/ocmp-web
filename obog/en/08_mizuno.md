---
layout: en/page_member
title: M. Mizuno
name:
  ja:
    first: 昌人
    last: 水野
    ruby:
      first: まさと
      last: みずの
  en:
    short: M. Mizuno
    first: Masato
    last: Mizuno
email: 
image: /assets/images/member/2021/Mizuno.jpg
position: Master
---

<!-- ## Email
 ![mizuno](/assets/images/email/mizuno-mail.png){: .email-image} -->


## Research


## Birthplace
Gifu


## Message
I enjoy making sweets.