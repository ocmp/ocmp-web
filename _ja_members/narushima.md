---
layout: ja/page_member
title: 成島哲也
name:
  ja:
    first: 哲也
    last: 成島
    ruby:
      first: てつや
      last: なるしま
  en:
    short: T. Narushima
    first: Tetsuya
    last: Narushima
email: 
image: /assets/images/member/2022/narushima.jpg
position: research staff
---


<div style="display: flex; align-items: flex-start; gap: 20px;">

<div>
  <h2>本務先</h2>
  <p>文部科学省 初等中等教育局 教科書調査官 (物理)</p>

  <h2>研究テーマ</h2>
  <p>物質のキラリティ・磁気特性のナノ光学研究</p>

  <h2>出身地</h2>
  <p>茨城県</p>

  <h2>一言</h2>
  <p>光と磁気、キラリティの関係を、実験的に可視化することにより、探究していきます。</p>
</div>


</div>

