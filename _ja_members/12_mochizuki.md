---
layout: ja/page_member
title: 望月颯一郎
name:
  ja:
    first: 颯一郎
    last: 望月
    ruby:
      first: そういちろう
      last: もちづき
  en:
    short: S. Mochizuki
    first: Soichiro
    last: Mochizuki
email: 
image: /assets/images/member/2022/mochizuki.jpg
position: Master
---

<!-- ## メールアドレス
![hiraoka](/assets/images/email/mochizuki-mail.png){: .email-image} -->


## 一言
Live life hard.