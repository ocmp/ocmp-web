---
layout: ja/page_member
title: 三国宏樹
name:
  ja:
    first: 宏樹
    last: 三国
    ruby:
      first: こうき
      last: みくに
  en:
    short: K. Mikuni
    first: Kouki
    last: Mikuni
email: 
image: /assets/images/member/2023/Mikuni.jpg
position: Master
---

<!-- ## メールアドレス
![hiraoka](/assets/images/email/hiraoka_mail.png){: .email-image} -->



## 一言
千葉大からきました