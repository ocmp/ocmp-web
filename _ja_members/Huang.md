---
layout: ja/page_member
title: 黃宥錡
name:
  ja:
    first: 宥錡
    last: 黃
    ruby:
      first: ゆうき
      last: こう
  en:
    short: 
    first: Yu-Chi
    last: Huang
email: 
image: /assets/images/member/2024/Huang.jpg
position: Master
---

<!-- ## メールアドレス
![iwase](/assets/images/email/iwase-mail.png){: .email-image} -->


<!-- ## 研究テーマ


## 出身地 -->


## 一言
愛されるのに必要なのは、自分を愛することだけ。