---
layout: ja/page_member
title: 宅見美春
name:
  ja:
    first: 美春
    last: 宅見
    ruby:
      first: みはる
      last: たくみ
  en:
    short: M. Takumi
    first: Miharu
    last: Takumi
email: 
image: /assets/images/member/2024/Takumi.jpg
position: Master
---

<!-- ## メールアドレス
![hiraoka](/assets/images/email/hiraoka_mail.png){: .email-image} -->



## 一言
食べることと動物を愛でることが生きがいです。