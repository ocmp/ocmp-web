---
layout: ja/page_member
title: 高橋俊介
name:
  ja:
    first: 俊介
    last: 高橋
    ruby:
      first: しゅんすけ
      last: たかはし
  en:
    short: S. Takahashi
    first: Shunsuke
    last: Takahashi
email: 
image: /assets/images/member/2023/Takahashi.jpg
position: Master
---

<!-- ## メールアドレス
![hiraoka](/assets/images/email/hiraoka_mail.png){: .email-image} -->




## 一言
禊祓の神産は宣い、禍祓の贖罪は誓う。