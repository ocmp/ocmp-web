---
layout: ja/page_member
title: 菊地勢
name:
  ja:
    first: 勢
    last: 菊地
    ruby:
      first: せい
      last: きくち
  en:
    short: S. Kikuchi
    first: Sei
    last: Kikuchi
email: 
image: /assets/images/member/2024/Say.jpg
position: Bachelor
---

<!-- ## メールアドレス
![hiraoka](/assets/images/email/mochizuki-mail.png){: .email-image} -->


## 一言
ぶっかましますか。