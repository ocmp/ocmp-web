---
layout: ja/page_member
title: 楠野楽到
name:
  ja:
    first: 楽到
    last: 楠野
    ruby:
      first: がくと
      last: くすの
  en:
    short: G. Kusuno
    first: Gakuto
    last: Kusuno
email: 
image: /assets/images/member/2022/kusuno.jpg
position: Master
---

<!-- ## メールアドレス
![kusuno](/assets/images/email/kusuno-mail.png){: .email-image} -->


<!-- ## 研究テーマ


## 出身地 -->


## 一言
趣味は3Dモデリングです。