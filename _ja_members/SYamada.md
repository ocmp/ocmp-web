---
layout: ja/page_member
title: 山田爽太
name:
  ja:
    first: 爽太
    last: 山田
    ruby:
      first: そうた
      last: やまだ
  en:
    short: S. Yamada
    first: Sota
    last: Yamada
email: 
image: /assets/images/member/2024/Yamada.jpg
position: Master
---

<!-- ## メールアドレス
![hiraoka](/assets/images/email/mochizuki-mail.png){: .email-image} -->


## 一言
横国から来ました。