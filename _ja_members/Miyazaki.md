---
layout: ja/page_member
title: 宮﨑航平
name:
  ja:
    first: 航平
    last: 宮﨑
    ruby:
      first: こうへい
      last: みやざき
  en:
    short: K. Miyazaki
    first: Miyazaki
    last: Kohei
email: 
image: /assets/images/member/2024/Miyazaki.jpg
position: Bachelor
---

<!-- ## メールアドレス
![hiraoka](/assets/images/email/hiraoka_mail.png){: .email-image} -->



## 一言
今年はライブに行きたいです。