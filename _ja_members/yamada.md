---
layout: ja/page_member
title: 山田 貴大
name:
  ja:
    first: 貴大
    last: 山田
    ruby:
      first: Kihiro
      last: Yamada
  en:
    short: K.Yamada
    first: Kihiro
    last: Yamada
email: yamada[at]phys.titch.ac.jp
image: /assets/images/member/2020/yamada.jpg
position: Assistant professor
---

## メールアドレス
![yamada](/assets/images/email/yamada_mail.png){: .email-image}



## 研究テーマ

(光)スピントロ二クス

## 出版物
[Google scholar](https://scholar.google.com/citations?user=TgTvzlUAAAAJ&hl=ja)


## 学歴

{:refdef .member-table}
|2008-2012|京都大学 理学部 理学科|
|2012-2014|京都大学 大学院理学研究科 化学専攻修士課程|
|2014-2017|京都大学 大学院理学研究科 化学専攻博士後期課程|
{: refdef}

## 職歴

{:refdef .member-table}
|2014.4-2017.3|日本学術振興会 特別研究員 DC1(京都大学小野研究室)|
|2017.4-2017.7|京都大学 化学研究所 博士研究員 (小野研究室)|
|2017.8-2019.7|Radboud University 博士研究員 (Prof. Theo Rasing group)|
|2019.8-2020.2|Radboud University 博士研究員 (Prof. Alexey Kimel group)|
|2020.3-現在|東京工業大学 理学院 物理学系 助教|
{: refdef}


## 出身地

岐阜県























