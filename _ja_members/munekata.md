---
layout: ja/page_member
title: 宗片比呂夫
name:
  ja:
    first: 比呂夫
    last: 宗片
    ruby:
      first: ひろお
      last: むねかた
  en:
    short: H. Munekata
    first: Hiro
    last: Munekata
email: 
image: /assets/images/member/2022/munekata.jpg
position: professor emeritus
---

## メールアドレス
![kainu](/assets/images/email/munekata_mail.png){: .email-image}

## 研究テーマ
光誘起電気伝導

## 出身地
（20年以上住んでいる自宅）：神奈川県横須賀市

## 一言
2024年度4月1日から佐藤研・研究員3年目となりました