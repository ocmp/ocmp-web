---
layout: en/page_member
title: K.Yamada
name:
  ja:
    first: 貴大
    last: 山田
    ruby:
      first: きひろ
      last: やまだ
  en:
    short: K.Yamada
    first: Kihiro
    last: Yamada
email: yamada[at]phys.titch.ac.jp
image: /assets/images/member/2020/yamada.jpg
position: Assistant professor
---

## Email
![yamada](/assets/images/email/yamada-mail.png){: .email-image}

## Research
(Opto)-spintronics


## Publication
[Google scholar](https://scholar.google.com/citations?user=TgTvzlUAAAAJ&hl=ja)


## Education

{:refdef .member-table}
|2008-2012|Faculty of Science, Kyoto University|
|2012-2014|Master course, Graduate School of Science, Kyoto University|
|2014-2017|Doctor course, Graduate School of Science, Kyoto University|
{: refdef}

## Professional experience

{:refdef .member-table}
|2014.4-2017.3|Research Fellow DC1, Japan Society for the Promotion of Science (Prof. Ono group, Kyoto University)|
|2017.4-2017.7|Postdoctral Researcher, Kyoto University (Prof. Teruo Ono group)|
|2017.8-2019.7|Postdoctral Researcher, Radboud University (Prof. Theo Rasing group)|
|2019.8-2020.2|Postdoctral Researcher, Radboud University (Prof. Alexey Kimel group)|
|2020.3-present|Assistant professor, Tokyo Institute of technology (Prof. Takuya Satoh group)|
{: refdef}
