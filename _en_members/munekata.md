---
layout: en/page_member
title: Hiro Munekata
name:
  ja:
    first: 比呂夫
    last: 宗片
    ruby:
      first: ひろお
      last: むねかた
  en:
    short: H. Munekata
    first: Hiro
    last: Munekata
email: 
image: /assets/images/member/2022/munekata.jpg
position: professor emeritus
---

## Email
 ![munekata](/assets/images/email/munekata_mail.png){: .email-image}


<!-- ## Research
Without portfolio -->

<!-- ## Birthplace
(Home for more than 20 years): Yokosuka City, Kanagawa Prefecture -->

## Message
I joined the Sato Lab as a researcher on April 1, 2022.