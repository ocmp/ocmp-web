---
layout: en/page_member
title: Y. Huang
name:
  ja:
    first: 
    last: 
    ruby:
      first: 
      last: 
  en:
    short: Y. Huang
    first: Yu-Chi
    last: Huang
email: 
image: /assets/images/member/2024/Huang.jpg
position: Master
---



## Message
To be loved is to be loved by yourself.