---
layout: en/page_member
title: T.Satoh
name:
  ja:
    first: 琢哉
    last: 佐藤
    ruby:
      first: たくや
      last: さとう
  en:
    short: T. Satoh
    first: Takuya
    last: Satoh
image: /assets/images/member/2022/satoh.jpg
position: Professor
---

## Education

{:refdef .member-table}
|1991-1994|Koyo-Gakuin Highschool|
|1994-1998|Department of Applied Physics, Faculty of Engineering, The University of Tokyo|
|1998-2000|Master course, Department of Applied Physics, Faculty of Engineering, The University of Tokyo|
|2000-2004|Doctor course, Department of Applied Physics, Faculty of Engineering, The University of Tokyo|
{: refdef}

## Professional experience

{:refdef .member-table}
|2000-2003|Research Fellow, Japan Society for the Promotion of Science (Miyano Laboratory, The University of Tokyo)|
|2003-2005|Research Fellow, Max Born Institute (Fiebig Laboratory), Germany|
|2006-2007|Research Fellow, SORST, Japan Science and Technology Agency (Komiyama Laboratory, The University of Tokyo)|
|2007-2014|Assistant Professor, Institute of Industrial Science, The University of Tokyo (Kuroda & Shimura Laboratory)|
|2010-2014|PRESTO Researcher, Japan Science and Technology Agency|
|2014-2019|Associate Professor, Department of Physics, Faculty of Sciences, Kyushu University|
|2015|Guest Professor, Department of Materials, ETH Zurich, Switzerland|
|2019-Present|Professor, Department of Physics, Tokyo Institute of Technology|
{: refdef}

## Awards

{:refdef .member-table}
|2009|Yayoi Award, Institute of Industrial Science, The University of Tokyo|
|2013|Young Scientist Award, The Physical Society of Japan|
|2014|Optics Awards, The Japan Society of Applied Physics|
|2015|The 14th Funai Information Technology Award, Funai Foundation for Information Technology|
|2015|The Young Scientists’ Prize, The Commendation for Science and Technology by the Minister of Education, Culture, Sports, Science and Technology|
|2015|The 17th Sir Martin Wood Prize, The Millennium Science Forum|
{: refdef}
