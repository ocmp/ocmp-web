---
layout: en/page_member
title: G. Kusuno
name:
  ja:
    first: 楽到
    last: 楠野
    ruby:
      first: がくと
      last: くすの
  en:
    short: G. Kusuno
    first: Gakuto
    last: Kusuno
email: 
image: /assets/images/member/2022/kusuno.jpg
position: Master
---

<!-- ## Email
 ![kusuno](/assets/images/email/kusuno-mail.png){: .email-image} -->

<!-- 
## Research


## Birthplace
 -->

## Message
My hobby is 3D modeling.