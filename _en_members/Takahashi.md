---
layout: en/page_member
title: S. Takahashi
name:
  ja:
    first: 
    last: 
    ruby:
      first: 
      last: 
  en:
    short: S. Takahashi
    first: Shunsuke
    last: Takahashi
email: 
image: /assets/images/member/2023/Takahashi.jpg
position: Master
---

<!-- ## メールアドレス
![hiraoka](/assets/images/email/hiraoka_mail.png){: .email-image} -->





## Message
For the king, for the land, for the mountains <br />
For the green valleys where dragons fly <br />
For the glory, the power to win the Black Lord <br />
I will search for the emerald sword <br />