---
layout: en/page_member
title: T. Narushima
name:
  ja:
    first: 哲也
    last: 成島
    ruby:
      first: てつや
      last: なるしま
  en:
    short: T. Narushima
    first: Tetsuya
    last: Narushima
email: 
image: /assets/images/member/2022/narushima.jpg
position: research staff
---


<!-- 
## 研究テーマ
物質のキラリティ・磁気特性のナノ光学研究

## 出身地
茨城県

## 一言
光と磁気，キラリティの関係を，実験的に可視化することにより，探求していきます。 -->


