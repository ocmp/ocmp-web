---
layout: en/page_member
title: Soichiro Mochizuki
name:
  ja:
    first: 颯一郎
    last: 望月
    ruby:
      first: そういちろう
      last: もちづき
  en:
    short: S. Mochizuki
    first: Soichiro
    last: Mochizuki
email: 
image: /assets/images/member/2022/mochizuki.jpg
position: Master
---

<!-- ## Email
 ![mizuno](/assets/images/email/mizuno-mail.png){: .email-image} -->

<!-- 
## Research


## Birthplace -->


## Message
Live life hard.