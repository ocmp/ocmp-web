@ECHO OFF

set RUBYOPT=-Ku

if "%1" == "setup" (
  bundle install
	goto end
)

if "%1" == "serve" (
  bundle exec jekyll s
	goto end
)

if "%1" == "build" (
  bundle exec jekyll build
	goto end
)

:end
