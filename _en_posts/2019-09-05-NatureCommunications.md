---
layout: en/post
title: Our article has been published in Applied Physics Express.
date: 2024.02.29.10:00:00
categories: paper
---

## Reference

1. [Nature Communications 10, 3995 (2019)](https://www.nature.com/articles/s41467-019-11961-9)
