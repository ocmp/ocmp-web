---
layout: en/post
title: New Laboratory made a start!
date: 2019.04.01.10:00:00
categories: news
---

Here in Tokyo Institute of Technology, Satoh laboratory was launched.<br>In this laboratory, we study ultrafast and coherent control of magnetic and ferroelectric materials by using femtosecond pulse laser.
