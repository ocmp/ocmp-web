---
layout: en/post
title: Our article has been published in Journal of Physics&#58; Condensed Matter
date: 2019.04.05.10:00:00
categories: paper
---

## Reference

1. [J. Phys.&#58; Condens. Matter 31, 275402 (2019)](https://doi.org/10.1088/1361-648X/ab1665)
