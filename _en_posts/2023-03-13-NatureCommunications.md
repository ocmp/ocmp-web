---
layout: en/post
title: Our article has been published in Nature Communications.
date: 2023.03.13.10:00:00
categories: paper
---

## Reference

1. [Nature Communications (2023)](https://www.nature.com/articles/s41467-023-37473-1)