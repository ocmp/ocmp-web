---
layout: ja/library
title: 磁性体の単位変換表
date: 2015.04.01.10:00
create_at: 2016.04.06.16:00
update_at: 2020.06.21.11:00
categories: library
---

# 磁性体の単位変換表

## 単位変換表のダウンロード

**※最新版を保ちたいため、二次配布禁止。**

2020年06月21日 11:00更新

- [PDFダウンロード](/assets/pdf/magnetic-material-units.pdf)
- [TeXファイルでダウンロード](/assets/data/magnetic-material-units.tex)

## 単位変換表

{:refdef .units-table}
||cgs Gauss単位系 | E-H対応 MKSA単位系 | E-B対応 SI単位系 |
| | 磁荷が磁場を作る |磁荷が磁場を作る | 電流が磁場を作る |
| Faradayの法則 | $\nabla\times\mathbf{E}=-\dfrac{1}{c}\dfrac{\partial\mathbf{B}}{\partial t}$ | $\nabla\times\mathbf{E}=-\dfrac{\partial\mathbf{B}}{\partial t}$ | $\nabla\times\mathbf{E}=-\dfrac{\partial\mathbf{B}}{\partial t}$ |
| Maxwell-Ampereの法則 | $\nabla\times\mathbf{H}=\dfrac{1}{c}\dfrac{\partial \mathbf{D}}{\partial t}+\dfrac{4\pi}{c}\mathbf{I}$ | $\nabla\times\mathbf{H}=\dfrac{\partial\mathbf{D}}{\partial t}+\mathbf{I}$ | $\nabla\times\mathbf{H}=\dfrac{\partial\mathbf{D}}{\partial t}+\mathbf{I}$ |
| Poisson方程式 | $\nabla\cdot\mathbf{D}=4\pi\rho$ | $\nabla\cdot\mathbf{D}=\rho$ | $\nabla\cdot\mathbf{D}=\rho$ |
| 磁場 | $\nabla\cdot\mathbf{B}=0$ | $\nabla\cdot\mathbf{B}=0$ | $\nabla\cdot\mathbf{B}=0$ |
| Biot-Savartの法則 | $d\mathbf{H} = \dfrac{1}{cr^2} \mathbf{I}\cdot d\mathbf{\ell}\times\hat{\mathbf{u}}$ | $d\mathbf{H}=\dfrac{1}{4\pi r^2}\mathbf{I}\cdot d\mathbf{\ell}\times\hat{\mathbf{u}}$ | $d\mathbf{H}=\dfrac{1}{4\pi r^2}\mathbf{I}\cdot d\mathbf{\ell}\times\hat{\mathbf{u}}$ |
| Lorentz力 | $q^{\mathrm{e}}(\mathbf{E}+\dfrac{1}{c}\mathbf{v}\times\mathbf{B})$ | $q^{\mathrm{e}}(\mathbf{E}+\mathbf{v}\times\mathbf{B})$ | $q^{\mathrm{e}}(\mathbf{E}+\mathbf{v}\times\mathbf{B})$ |
| constitutive relation | $\mathbf{D}=\varepsilon \mathbf{E}=\mathbf{E}+4\pi\mathbf{P}$ | $\mathbf{D}=\varepsilon \mathbf{E}=\varepsilon_0 \mathbf{E}+ \mathbf{P}$ | $\mathbf{D}=\varepsilon \mathbf{E}=\varepsilon_0 \mathbf{E}+ \mathbf{P}$ |
| 電気感受率 | $\mathbf{P}=\chi^{\mathrm{e}} \mathbf{E},~ \varepsilon = 1+4\pi\chi^{\mathrm{e}}$ | $\mathbf{P}=\varepsilon_0 \chi^{\mathrm{e}} \mathbf{E},~ \dfrac{\varepsilon}{\varepsilon_0}=1+\chi^{\mathrm{e}}$ | $\mathbf{P}=\varepsilon_0 \chi^{\mathrm{e}} \mathbf{E},~ \dfrac{\varepsilon}{\varepsilon_0}=1+\chi^{\mathrm{e}}$ |
| constitutive relation | $\mathbf{B}~[\mathrm{G}] = \mu \mathbf{H} = \mathbf{H}~[\mathrm{Oe}] +4\pi\mathbf{M}~\left[\mathrm{\dfrac{emu}{cm^3}}\right]$ | $\mathbf{B}~[\mathrm{T}] = \mu \mathbf{H} = \mu_0 \mathbf{H}+\mathbf{M}~\left[\mathrm{\dfrac{N}{A\cdot m}}=\mathrm{\dfrac{Wb}{m^2}}\right]$ | $\mathbf{B}~[\mathrm{T}] = \mu \mathbf{H} = \mu_0(\mathbf{H}+\mathbf{M})= \mu_0\mathbf{H}+\mathbf{J}~\left[\mathrm{\dfrac{N}{A\cdot m}}=\mathrm{\dfrac{Wb}{m^2}}\right]$ |
| 磁気感受率 | $\mathbf{M}=\chi^{\mathrm{m}} \mathbf{H}, ~ \mu = 1+4\pi \chi^{\mathrm{m}}$ | $\mathbf{M}=\mu_0\chi^{\mathrm{m}} \mathbf{H}, ~ \dfrac{\mu}{\mu_0} = 1 + \chi^{\mathrm{m}}$ | $\mathbf{M}=\chi^{\mathrm{m}} \mathbf{H}, ~ \dfrac{\mu}{\mu_0} = 1 + \chi^{\mathrm{m}}$ |
| 誘電率 | $\varepsilon_0=1$ | $\varepsilon_0\simeq 8.854\times{10^{-12}}~\left[\mathrm{\dfrac{C^2}{N\cdot m^2}}=\mathrm{\dfrac{F}{m}}\right]$ | $\varepsilon_0\simeq 8.854\times{10^{-12}}~\left[\mathrm{\dfrac{C^2}{N\cdot m^2}}=\mathrm{\dfrac{F}{m}}\right]$ |
| 透磁率 | $\mu_0=1$ | $\mu_0 = 4\pi\times{10^{-7}}~\left[\mathrm{\dfrac{Wb}{A\cdot{m}}}=\mathrm{\dfrac{N}{A^2}}=\mathrm{\dfrac{H}{m}}\right]$ | $\mu_0 = 4\pi\times{10^{-7}}~\left[\mathrm{\dfrac{Wb}{A\cdot{m}}}=\mathrm{\dfrac{N}{A^2}}=\mathrm{\dfrac{H}{m}}\right]$ |
| 電荷 | $q^{\mathrm{e}}~[\mathrm{esu}=\mathrm{statC}]$ | $q^{\mathrm{e}}~[\mathrm{C}]$ | $q^{\mathrm{e}}~[\mathrm{C}]$ |
| 磁荷 | $q^{\mathrm{m}}~[\mathrm{emu}]$ | $q^{\mathrm{m}}~\left[\mathrm{Wb}=\mathrm{\dfrac{N\cdot m}{A}}\right]$ | $\left(q^{\mathrm{m}}~[\mathrm{A\cdot m}]\right)$ |
| 電場 | $E=\dfrac{q_1^e}{r^2}~\left[\mathrm{\dfrac{statV}{cm}}\right]$ | $E=\dfrac{1}{4\pi\varepsilon_0}\dfrac{q_1^e}{r^2}~\left[\mathrm{\dfrac{N}{C}}=\mathrm{\dfrac{V}{m}}\right]$ | $E=\dfrac{1}{4\pi\varepsilon_0}\dfrac{q_1^e}{r^2}~\left[\mathrm{\dfrac{N}{C}}=\mathrm{\dfrac{V}{m}}\right]$ |
| 磁場 | $H=\dfrac{q_1^{\mathrm{m}}}{r^2}~[\mathrm{Oe}]$ | $H=\dfrac{1}{4\pi\mu_0}\dfrac{q_1^{\mathrm{m}}}{r^2}~\left[\mathrm{\dfrac{A}{m}}=\mathrm{\dfrac{N}{Wb}}\right]$ | $\left(H=\dfrac{1}{4\pi}\dfrac{q_1^{\mathrm{m}}}{r^2}~\left[\mathrm{\dfrac{A}{m}}\right]\right)$ |
| 電荷によるCoulomb力 | $\dfrac{q_1^{\mathrm{e}}q_2^{\mathrm{e}}}{r^2}=Eq_2^{\mathrm{e}}~[\mathrm{dyne}]$ | $\dfrac{1}{4\pi\varepsilon_0}\dfrac{q_1^{\mathrm{e}}q_2^{\mathrm{e}}}{r^2} = Eq_2^{\mathrm{e}}~[\mathrm{N}]$ | $\dfrac{1}{4\pi\varepsilon_0}\dfrac{q_1^{\mathrm{e}}q_2^{\mathrm{e}}}{r^2} = Eq_2^{\mathrm{e}}~[\mathrm{N}]$ |
| 磁荷によるCoulomb力 | $\dfrac{q_1^{\mathrm{m}}q_2^{\mathrm{m}}}{r^2}=Hq_2^{\mathrm{m}}~[\mathrm{dyne}]$ | $\dfrac{1}{4\pi\mu_0}\dfrac{q_1^{\mathrm{m}}q_2^{\mathrm{m}}}{r^2}=Hq_2^{\mathrm{m}}~[\mathrm{N}]$ | $\dfrac{\mu_0}{4\pi}\dfrac{q_1^{\mathrm{m}}q_2^{\mathrm{m}}}{r^2}=\mu_0 Hq_2^{\mathrm{m}}~[\mathrm{N}]$ |
| 磁気モーメント | $\mathbf{m}=\dfrac{1}{c}I\mathbf{S}~\mathrm{or}~q^{\mathrm{m}}\mathbf{r} ~ \left[\mathrm{emu}=\mathrm{\dfrac{erg}{Oe}} \right]$ | $\mathbf{m}=\mu_0 I\mathbf{S}~\mathrm{or}~q^{\mathrm{m}}\mathbf{r} ~ [\mathrm{Wb \cdot m}]$ | $\mathbf{m}=I\mathbf{S}~(\mathrm{or}~q^{\mathrm{m}}\mathbf{r}) ~ \left[\mathrm{A\cdot m^2}=\mathrm{\dfrac{J}{T}} \right]$ |
| 磁化 | $\mathbf{M}=\dfrac{\mathbf{m}}{V}~\left[\mathrm{\dfrac{emu}{cm^3}}\right]$ | $\mathbf{M}=\dfrac{\mathbf{m}}{V}~\left[\mathrm{\dfrac{Wb}{m^2}}\right]$ | $\mathbf{M}=\dfrac{\mathbf{m}}{V}~\left[\mathrm{\dfrac{A}{m}}\right],~\mathbf{J}=\mu_0 \mathbf{M}~[\mathrm{T}]$ |
| Zeemanエネルギー | $-\mathbf{m}\cdot\mathbf{H}~[\mathrm{erg}]$ | $-\mathbf{m}\cdot\mathbf{H}~[\mathrm{J}]$ | $-\mathbf{m}\cdot\mathbf{B}~[\mathrm{J}]$ |
| 磁束 | $\Phi=HS~[\mathrm{Oe \cdot cm^2}=\mathrm{Mx}]$ | $\Phi=\mu_0 HS~[\mathrm{Wb}=\mathrm{V\cdot s}=\mathrm{H\cdot A}]$ | $\Phi=\mu_0 HS~[\mathrm{Wb}=\mathrm{V\cdot s}=\mathrm{H\cdot A}]$ |
| 誘導起電力 | $-\dfrac{1}{c} \dfrac{\partial\Phi}{\partial t}~\left[\mathrm{\dfrac{Mx}{cm}} = \mathrm{V}\right]$ | $-\dfrac{\partial\Phi}{\partial t}~\left[\mathrm{\dfrac{Wb}{s}} = \mathrm{V}\right]$ | $-\dfrac{\partial\Phi}{\partial t}~\left[\mathrm{\dfrac{Wb}{s}} = \mathrm{V}\right]$ |
| Poyntingベクトル | $\dfrac{c}{4\pi}(\mathbf{E}\times\mathbf{H})~\left[\mathrm{\dfrac{erg}{s \cdot cm^2}}\right]$ | $\mathbf{E}\times\mathbf{H}~\left[\mathrm{\dfrac{J}{s \cdot m^2}}\right]$ | $\mathbf{E}\times\mathbf{H}~\left[\mathrm{\dfrac{J}{s \cdot m^2}}\right]$ |
| 電磁場エネルギー密度 | $\dfrac{1}{8\pi}(E^2+B^2)$ | $\dfrac{1}{2}\left(\varepsilon_0 E^2+\dfrac{B^2}{\mu_0}\right)~\left[\mathrm{\dfrac{J}{m^3}}\right]$ | $\dfrac{1}{2}\left(\varepsilon_0 E^2+\dfrac{B^2}{\mu_0}\right)~\left[\mathrm{\dfrac{J}{m^3}}\right]$ |
| 電子の電荷量 | $e=4.803\times10^{-10}~[\mathrm{statC}]$ | $e=1.602\times 10^{-19}~[\mathrm{C}]$ | $e=1.602\times 10^{-19}~[\mathrm{C}]$ |
| Bohr磁子 | $\mu_{\mathrm{B}}=\dfrac{e \hbar}{2mc}=9.274\times 10^{-21}~\left[\mathrm{emu}=\mathrm{Oe \cdot cm^3}=\mathrm{\dfrac{erg}{Oe}}\right]$ | $\mu_{\mathrm{B}}=\dfrac{\mu_0 e \hbar}{2m}=1.165\times 10^{-29}~\left[\mathrm{Wb \cdot m}\right]$ | $\mu_{\mathrm{B}}=\dfrac{e \hbar}{2m}=9.274\times 10^{-24}\,\left[\mathrm{A \cdot m^2}=\mathrm{\dfrac{J}{T}}\right]$ |
{: refdef}

## 磁性に関する物理量と各単位系への換算

### [磁性入門 志賀 正幸](http://www.amazon.co.jp/%E7%A3%81%E6%80%A7%E5%85%A5%E9%96%80%E2%80%95%E3%82%B9%E3%83%94%E3%83%B3%E3%81%8B%E3%82%89%E7%A3%81%E7%9F%B3%E3%81%BE%E3%81%A7-%E6%9D%90%E6%96%99%E5%AD%A6%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA-%E5%BF%97%E8%B3%80-%E6%AD%A3%E5%B9%B8/dp/4753656306)

{:refdef .units-table}
| 物理量 | 記号 | MKS(E-H対応) | cgs単位 | MKS→cgs×定数 | cgs→MKS×定数 |
| 磁場 | $H$ | $\mathrm{A/m}$ | $\mathrm{Oe}$ | $4\pi\times10^{-3}$ | $10^3/4\pi$ |
| 磁気モーメント | $M$ | $\mathrm{Wb\cdot m}$ | $\mathrm{emu}$ | $10^{10}/4\pi$ | $4\pi\times 10^{-10}$ |
| 磁化 | $I$ | $\mathrm{Wb/m^2(=T)}$ | $\mathrm{emu/cm^3}$ | $10^4/4\pi$ | $4\pi\times 10^{-4}$ |
| 磁束 | $\Phi$ | $\mathrm{Wb}$ | $\mathrm{Mx}$ | $10^8$ | $10^{-8}$ |
| 磁束密度 | $B$ | $\mathrm{T (=Wb/m^2)}$ | $\mathrm{G}$ | $10^4$ | $10^{-4}$ |
| 帯磁率/体積 | $\chi$ | $\mathrm{H/m}$ | 無次元
$(\mathrm{emu/cm^3})$ | $10^7/(4\pi)^2$ | $(4\pi)^2\times 10^{-7}$ |
| 帯磁率/質量 | $\chi_m$ | $\mathrm{H\cdot m^2/kg}$ | $\mathrm{cm^3/g}$ | $10^{10}/(4\pi)^2$ | $(4\pi)^2\times10^{-10}$ |
| 比帯磁率/体積 | $\bar{\chi}$ | 無次元 | $\mathrm{emu/cm^3}$ | $1/4\pi$ | $4\pi$ |
| 比帯磁率/質量 | $\bar{\chi}_m$ | $\mathrm{m^3/kg}$ | $\mathrm{cm^3/g}$ | $10^3/4\pi$ | $4\pi\times 10^{-3}$ |
| 透磁率 | $\mu$ | $\mathrm{H/m}$ | 無次元 | $10^7/4\pi$ | $4\pi\times 10^{-7}$ |
| 比透磁率 | $\bar{\mu}$ | 無次元 | 無次元 | $1$ | $1$ |
{: refdef}

## 磁性に関する物理量とSI単位系(E-B対応MKS)での単位およびcgs単位系への換算

### [磁性入門 志賀 正幸](http://www.amazon.co.jp/%E7%A3%81%E6%80%A7%E5%85%A5%E9%96%80%E2%80%95%E3%82%B9%E3%83%94%E3%83%B3%E3%81%8B%E3%82%89%E7%A3%81%E7%9F%B3%E3%81%BE%E3%81%A7-%E6%9D%90%E6%96%99%E5%AD%A6%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA-%E5%BF%97%E8%B3%80-%E6%AD%A3%E5%B9%B8/dp/4753656306)

{:refdef .units-table}
| 物理量 | 記号 | SI | cgs単位 | SI→cgs×定数 | cgs→SI×定数 |
| 磁気モーメント | $M$ | $\mathrm{A\cdot m^3 (=J/T)}$ | $\mathrm{emu}$ | $10^3$ | $10^{-3}$ |
| 磁化 | $I$ | $\mathrm{A/m}$ | $\mathrm{emu/cm^3}$ | $10^{-3}$ | $10^3$ |
| 磁気分極 | $J$ | $\mathrm{Wb/m^2 (=T)}$ | $\mathrm{emu/cm^3}$ | $\mathrm{10^4/4\pi}$ | $\mathrm{4\pi\times 10^{-4}}$ |
| 帯磁率/体積 | $\chi$ | 無次元 | 無次元 | $1/4\pi$ | $4\pi$ |
| 帯磁率/質量 | $\chi_m$ | $\mathrm{m^3/kg}$ | $\mathrm{cm^3/g}$ | $10^3/4\pi$ | $4\pi \times 10^3$ |
{: refdef}